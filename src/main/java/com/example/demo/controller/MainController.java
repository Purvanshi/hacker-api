package com.example.demo.controller;

import com.example.demo.Model.UserStory;
import com.example.demo.Service.UserStoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

@RestController
@Api("This is service manages all Hacker APi Stories")
@RequestMapping(name = "hacker api", value = "/api")
@EnableWebMvc
public class MainController {

    @Autowired
    UserStoryService userStoryService;

    @ApiOperation(value = "save data from firebase to mongodb")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully fetched"),
            @ApiResponse(code = 400, message = "Bad Request!"),
            @ApiResponse(code = 401, message = "Not authorized!"),
            @ApiResponse(code = 403, message = "Forbidden!!!"),
            @ApiResponse(code = 404, message = "Not found!!!")
    })
    @GetMapping(value = "/fetch-user-story", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity fetchUserStories() {
        userStoryService.saveStories();
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "fetch top 10 stories")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully fetched"),
            @ApiResponse(code = 400, message = "Bad Request!"),
            @ApiResponse(code = 401, message = "Not authorized!"),
            @ApiResponse(code = 403, message = "Forbidden!!!"),
            @ApiResponse(code = 404, message = "Not found!!!")
    })
    @GetMapping(value = "/best-stories", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity bestUserStories() {
        List<UserStory> userStories = userStoryService.getBestStories();
        return ResponseEntity.ok(userStories);
    }

    @ApiOperation(value = "fetch past stories stories")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully fetched"),
            @ApiResponse(code = 400, message = "Bad Request!"),
            @ApiResponse(code = 401, message = "Not authorized!"),
            @ApiResponse(code = 403, message = "Forbidden!!!"),
            @ApiResponse(code = 404, message = "Not found!!!")
    })
    @GetMapping(value = "/past-stories", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity bestPastStories(
            @ApiParam(name = "pageNumber", value = "The page number", defaultValue = "0")
            @RequestParam(name = "pageNumber", defaultValue = "0") String pageNumber,
            @ApiParam(name = "pageSize", value = "The page size", defaultValue = "25")
            @RequestParam(name = "pageSize", defaultValue = "25") String pageSize

            ) {
        Page<UserStory> userStories = userStoryService.getPastStories(Integer.valueOf(pageNumber), Integer.valueOf(pageSize));
        return ResponseEntity.ok(userStories);
    }

}