package com.example.demo.Service;

import com.example.demo.Model.UserStory;
import com.example.demo.Repository.UserStoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@Service
public class UserStoryService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    UserStoryRepository userStoryRepository;

    public void saveStories() {

        String url = "https://hacker-news.firebaseio.com/v0/beststories.json";

        String url1 = "https://hacker-news.firebaseio.com/v0/item/192327.json";

        String data = "";

        ResponseEntity<UserStory> response
                = restTemplate.getForEntity(url1, UserStory.class);
        List<Integer> users
                = (List<Integer>) restTemplate.getForEntity(url, Object.class).getBody();

        this.saveUserStory(users);

    }

    public void saveUserStory(List<Integer> users) {

        String url1 = "https://hacker-news.firebaseio.com/v0/item/";

        for (int user : users) {
            UserStory response
                    = restTemplate.getForEntity(url1 + user + ".json", UserStory.class).getBody();
            userStoryRepository.save(response);
        }

    }

    public List<UserStory> getBestStories()
    {
        ZoneId zoneId = ZoneId.systemDefault();
        long endTime = LocalDateTime.now().atZone(zoneId).toEpochSecond();
        long startTime = LocalDateTime.now().atZone(zoneId).minusMinutes(15).toEpochSecond();

       return  userStoryRepository.fetchStoriesByTime(startTime,endTime);

    }

    public Page<UserStory> getPastStories(int pageNumber, int pageSize)
    {
        return userStoryRepository.findAll(
                 PageRequest.of(pageNumber,pageSize));
    }


}