package com.example.demo.Repository;

import com.example.demo.Model.UserStory;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserStoryRepository extends MongoRepository<UserStory, String>,CustomUserStoryRepository {



}
