package com.example.demo.Repository;

import com.example.demo.Model.UserStory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.Arrays;
import java.util.List;

public class CustomUserStoryRepositoryImpl implements CustomUserStoryRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<UserStory> fetchStoriesByTime(long startTime, long endTime) {        SortOperation sortOperation = Aggregation.sort(Sort.by(UserStory.Constants.SCORE).descending());
        Criteria criteria = Criteria.where(UserStory.Constants.TIME).gte(startTime).lte(endTime);
        LimitOperation limitOperation = new LimitOperation(10);
        MatchOperation matchOperation = Aggregation.match(criteria);


        TypedAggregation<UserStory> typedAggregation = Aggregation.newAggregation(UserStory.class,
                Arrays.asList(
                        matchOperation,
                        sortOperation,
                        limitOperation
                ));

        AggregationResults<UserStory> aggregate = mongoTemplate.aggregate(typedAggregation, UserStory.class, UserStory.class);


        return aggregate.getMappedResults();

    }
}
