package com.example.demo.Repository;


import com.example.demo.Model.UserStory;

import java.util.List;

public interface CustomUserStoryRepository  {

    List<UserStory> fetchStoriesByTime(long startTime , long endTime);
}
